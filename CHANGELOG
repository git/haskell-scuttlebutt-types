scuttlebutt-types (0.4.1) UNRELEASED; urgency=medium

  * Fix typo in json parse error message.

 -- Joey Hess <id@joeyh.name>  Sun, 21 Oct 2018 19:46:22 -0400

scuttlebutt-types (0.4.0) unstable; urgency=medium

  * Simplified Post type (API change)
  * Support parsing a Post with a root or branch that has a value
    that is not a valid link.
  * Fix parsing of Post recps.
  * Mention renamed to UserLink. (API change)

 -- Joey Hess <id@joeyh.name>  Mon, 26 Feb 2018 14:32:59 -0400

scuttlebutt-types (0.3.0) unstable; urgency=medium

  * Handle about messages whose image field is a string linking to a blob,
    rather than an object.
  * Removed ToJSON instance for Message, until something can be done about
    https://github.com/ssbc/ssb-feed/issues/11
  * Added contentType.
  * Support parsing a Post with multiple branches.
  * Support parsing a Post when its mentions field is an object rather than
    an array.
  * Changes to Message data type to support above changes. (API change)
  * Support parsing a Post that mentions something that is a not a valid
    ssb link.
  * Added description to About.

 -- Joey Hess <id@joeyh.name>  Fri, 06 Oct 2017 14:48:37 -0400

scuttlebutt-types (0.2.0) unstable; urgency=medium

  * Improved Link types.
  * Added Hash type.
  * Some API changes.

 -- Joey Hess <id@joeyh.name>  Thu, 05 Oct 2017 17:40:48 -0400

scuttlebutt-types (0.1.0) unstable; urgency=low

  * First release, split off from haskell-scuttlebutt.

 -- Joey Hess <id@joeyh.name>  Thu, 05 Oct 2017 12:47:48 -0400

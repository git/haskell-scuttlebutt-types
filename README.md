Secure Scuttlebutt is a database of unforgeable append-only feeds,
optimized for efficient replication for peer to peer protocols.
<https://www.scuttlebutt.nz/>

This library contains Haskell data types for common Secure Scuttlebutt
messages, including JSON deserialization.

(JSON serialization support is blocked by
<https://github.com/ssbc/ssb-feed/issues/11>)

Git repositories:

* https://git.joeyh.name/git/haskell-scuttlebutt-types.git
* git-ssb repository:
  ssb://%h2FnIacUWXelN8YGY0NcShMjQCqkcDF8XVpW64LgTHE=.sha256

Contributions welcome! Please use git-ssb or email for making pull requests
and issue tracking. Also you can find fellow haskellers on the #haskell
channel on SSB, and discuss this library there.

Other Haskell Scuttlebutt resources:

* The haskell-scuttlebutt git repository is built on top of
  scuttlebutt-types, and has dreams of one day growing up to be a
  real scuttlebutt client. It git-ssb repository is here:
  ssb://%Nk2lTG8YHweKZEAfDrmzyjrkHPX1C9jHmYqc6csTCdU=.sha256
* <https://john.server.ky/src/ssb-haskell> implements the protocol
  up to the client side handshake as of this writing.

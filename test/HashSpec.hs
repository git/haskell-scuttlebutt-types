{-# LANGUAGE OverloadedStrings #-}

module HashSpec where

import Ssb.Types.Hash
import Test.Hspec
import Data.ByteString

base64sha256 :: ByteString
base64sha256 = "MPB9vxHO0pvi2ve2wh6Do05ZrV7P6ZjUQ+IEYnzLfTs="

spec :: Spec
spec = describe "sha256 hash" $ do
	it "can be deserialized and serialized from base64 bytestrings" $ do
		let (Just hash) = parseSha256 base64sha256
		(formatHash hash) `shouldBe` base64sha256

{-# LANGUAGE OverloadedStrings #-}

module KeySpec where

import Ssb.Types.Key
import Test.Hspec
import Data.ByteString

base64PublicKey :: ByteString
base64PublicKey = "opBinLmYiID9SMEoZJPH60LmduSYAR7J00P7/Gj9vHw="

spec :: Spec
spec = describe "public key" $ do
	it "can be deserialized and serialized from base64 bytestrings" $ do
		let (Just publicKey) = parseEd25519PublicKey base64PublicKey
		(formatPublicKey publicKey) `shouldBe` base64PublicKey

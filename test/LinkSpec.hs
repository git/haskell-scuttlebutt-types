{-# LANGUAGE OverloadedStrings #-}

module LinkSpec where

import Ssb.Types.Link
import Ssb.Types.Key
import Test.Hspec
import Data.Text
import Data.Maybe

feedLink :: Text
feedLink = "@LA9HYf5rnUJFHHTklKXLLRyrEytayjbFZRo76Aj/qKs=.ed25519"

spec :: Spec
spec = do
	describe "Link" $ do
		it "parses" $ do
			let (Right l) = parseLink feedLink
			linkSigil l `shouldBe` '@'
			linkTo l `shouldBe` "LA9HYf5rnUJFHHTklKXLLRyrEytayjbFZRo76Aj/qKs="
			linkTag l `shouldBe` "ed25519"
			formatLink l `shouldBe` feedLink
	describe "FeedLink" $ do
		it "parses" $ do
			let (Right l) = fromLink =<< parseLink feedLink
			unFeedLink l `shouldBe` fromJust (parseEd25519PublicKey "LA9HYf5rnUJFHHTklKXLLRyrEytayjbFZRo76Aj/qKs=")
